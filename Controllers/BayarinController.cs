using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BayarinApi.Services;


namespace BayarinApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BayarinController : ControllerBase
    {

        private ILogger _logger;
        private IBayarinService _service;
        
        public BayarinController(ILogger<BayarinController> logger, IBayarinService service)
        {
            _logger = logger;
            _service = service;
            
        }

        [HttpGet("/api/GetAllUser")]
        public ActionResult<List<User>> GetUsers()
        {
            return _service.GetUser();
        }

        [HttpGet("/api/GetUserDetail/{id}")]
        public ActionResult<User> GetUserAccount(Guid id)
        {            
            return _service.GetUser(id);
        }

        [HttpGet("/api/GetUserBalance/{id}")]
        public ActionResult<double> GetBalance(Guid id)
        {            
            return _service.GetUser(id).Balance;
        }

        [HttpPut("/api/AddFund/{id}")]
        public ActionResult<User> AddUserFund(Guid id, double amount)
        {
            var u = new User(){ Id = id, Balance = amount, IsAddFund = true };            
            return _service.UpdateUser(id, u);
        }   

        [HttpPut("/api/UpdateUserPayment/{userid}/{id}")]
        public ActionResult<Payment> UpdatePayment(Guid userid, Guid id, Payment p)
        {
            return _service.UpdatePayment(userid, id, p);
        }
    
    }
}
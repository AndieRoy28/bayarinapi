using System;
using System.Collections.Generic;

namespace BayarinApi.Services
{
    public class BayarinService: IBayarinService
    {

        private List<User> _userItems;

        private  string[] _paymentType = new[]
        {
            "Meralco", "Manila Water", "Laguna Water", "Converge", "Sky Broadband", "PLDT", "Bayantel", "Smart", "Globe", "Sun Cellular"
        };

        public BayarinService()
        {
            var r = new Random();
            _userItems = new List<User>();
            
            //User Data
            _userItems.Add(new User() { Id = Guid.NewGuid(),  UserName = "Andie", Email = "andie@gmail.com", Balance = r.Next(1000, 10000) });
            _userItems.Add(new User() { Id = Guid.NewGuid(),  UserName = "Joy", Email = "joy@gmail.com", Balance = r.Next(1000, 10000) });
            // _userItems.Add(new User() { Id = Guid.NewGuid(),  UserName = "Paco", Email = "paco@gmail.com", Balance = r.Next(1000, 10000) });
            // _userItems.Add(new User() { Id = Guid.NewGuid(),  UserName = "Haidee", Email = "haidee@gmail.com", Balance = r.Next(1000, 10000) });
            // _userItems.Add(new User() { Id = Guid.NewGuid(),  UserName = "Andrea", Email = "andrea@gmail.com", Balance = r.Next(1000, 10000) });
            // _userItems.Add(new User() { Id = Guid.NewGuid(),  UserName = "Andy", Email = "andy@gmail.com", Balance = r.Next(1000, 10000) });
            // _userItems.Add(new User() { Id = Guid.NewGuid(),  UserName = "Garcia", Email = "garcia@gmail.com", Balance = r.Next(1000, 10000) });
            // _userItems.Add(new User() { Id = Guid.NewGuid(),  UserName = "Ruth", Email = "ruth@gmail.com", Balance = r.Next(1000, 10000) });
            // _userItems.Add(new User() { Id = Guid.NewGuid(),  UserName = "Pena", Email = "pena@gmail.com", Balance = r.Next(1000, 10000) });
            // _userItems.Add(new User() { Id = Guid.NewGuid(),  UserName = "Torio", Email = "torio@gmail.com", Balance = r.Next(1000, 10000) });
        
            for (int i = 0; i < _userItems.Count; i++)
            {
                _userItems[i].Payments.Add(AddPayment(_userItems[i].Id));
                _userItems[i].Payments.Add(AddPayment(_userItems[i].Id));
                _userItems[i].Payments.Add(AddPayment(_userItems[i].Id));
                _userItems[i].Payments.Add(AddPayment(_userItems[i].Id));
                _userItems[i].Payments.Sort((x, y) => y.DateCreated.CompareTo(x.DateCreated));
            }
        }

        public List<User> GetUser()
        {
            return _userItems;
        }

        public User GetUser(Guid id)
        {
            var usertItem = new User();
            for (var index = _userItems.Count - 1; index >= 0; index--)
            {
                if (_userItems[index].Id == id)
                {
                    usertItem = _userItems[index];
                }
            }
            return usertItem;
        }

        

        public User AddUser(User usertItem)
        {
            try
            {
                usertItem.Id = Guid.NewGuid();
                _userItems.Add(usertItem);
            }
            catch (System.Exception)
            {
                
            }
            
            return usertItem;
        }

        public User UpdateUser(Guid id, User usertItem)
        {
            try
            {
                for (var index = _userItems.Count - 1; index >= 0; index--)
                {
                    if (_userItems[index].Id == id)
                    {
                        if(usertItem.IsAddFund){
                            _userItems[index].Balance = usertItem.Balance + _userItems[index].Balance;
                        }
                        else{
                            _userItems[index] = usertItem;
                        }    
                        
                    }
                }
            }
            catch (System.Exception)
            {
                
            }
            
            return usertItem;
        }

        public Guid DeleteUser(Guid id)
        {
            try
            {
                for (var index = _userItems.Count - 1; index >= 0; index--)
                {
                    if (_userItems[index].Id == id)
                    {
                        _userItems.RemoveAt(index);
                    }
                }
            }
            catch (System.Exception)
            {
                
            }
            

            return id;
        }


        public List<Payment> GetPayment(Guid userId)
        {
            var usertItem = new User();
            for (var index = _userItems.Count - 1; index >= 0; index--)
            {
                if (_userItems[index].Id == userId)
                {
                    usertItem = _userItems[index];
                }
            }
            return usertItem.Payments;
        }


        public Payment AddPayment(Guid userId)
        {
            var r = new Random();
            var d = DateTime.Now.AddDays(r.Next(-300, -1));
            var paymentItem = new Payment{
                Id = Guid.NewGuid(),
                UserId = userId,
                Name = _paymentType[r.Next(_paymentType.Length)],
                DateCreated = d,
                DateUpdated = d,
                Amount = r.Next(100, 5000),
                Status = PaymentStatus.Active.ToString(),
                Reason = ""
            };            

            return paymentItem;
        }

        public Payment AddPayment(Guid userId, Payment paymentItem)
        {
            try
            {
                paymentItem.Id = Guid.NewGuid();
                var usertItem = GetUser(userId);
                usertItem.Payments.Add(paymentItem);
                
                UpdateUser(userId, usertItem);
            }
            catch (System.Exception)
            {
                
            }
            

            return paymentItem;
        }

        public Payment UpdatePayment(Guid userId, Guid id,Payment paymentItem)
        {
            try
            {
                var usertItem = GetUser(userId);

                for (var i = usertItem.Payments.Count - 1; i >= 0; i--)
                {
                    if (usertItem.Payments[i].Id == id)
                    {
                        if(paymentItem.Status == "Paid"){
                            if(usertItem.Balance > usertItem.Payments[i].Amount){
                                //update balance
                                usertItem.Payments[i].Status = "Closed";
                                usertItem.Balance = usertItem.Balance - usertItem.Payments[i].Amount;
                                usertItem.Payments[i].Reason = "Payment Success";
                            }
                            else  {
                                usertItem.Payments[i].Status = "Pending";
                                usertItem.Payments[i].Reason = "Not enough balance";
                            }
                            
                        }
                        else if(paymentItem.Status == "Closed"){
                            usertItem.Payments[i].Reason = "Payment Closed";
                        }
                        else{
                            usertItem.Payments[i].Status = "Pending";
                            usertItem.Payments[i].Reason = "Payment is on pending";
                        }
                        
                        //usertItem.Payments[i] = paymentItem;
                    }
                }
                
                UpdateUser(userId, usertItem);
            }
            catch (System.Exception)
            {
                
            }            

            return paymentItem;
        }

    }
}
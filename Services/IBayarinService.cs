using System;
using System.Collections.Generic;

namespace BayarinApi.Services
{
    public interface IBayarinService
    {
        public List<User> GetUser();

        public User GetUser(Guid id);

        public User AddUser(User userItem);

        public User UpdateUser(Guid id, User userItem);

        public Guid DeleteUser(Guid id);

        public List<Payment> GetPayment(Guid userId);

        public Payment AddPayment(Guid userId);

        public Payment AddPayment(Guid userId, Payment paymentItem);        

        public Payment UpdatePayment(Guid userId, Guid id, Payment paymentItem);
    }
}
# README #

For this .net project I provided a API with the below endpoints. 


#### Endpoints & Schema

##### GET `/api/GetAllUser`

```JSON
[
  {
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "userName": "string",
    "email": "string",
    "payments": [
      {
        "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        "name": "string",
        "dateCreated": "2021-03-10T22:26:34.468Z",
        "dateUpdated": "2021-03-10T22:26:34.468Z",
        "amount": 0,
        "status": "string",
        "reason": "string",
        "userId": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
      }
    ],
    "balance": 0,
    "isAddFund": true
  }
]
```

##### GET `/api/GetUserDetail/{id}`

```JSON
{
  "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "userName": "string",
  "email": "string",
  "payments": [
    {
      "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "name": "string",
      "dateCreated": "2021-03-10T22:27:43.642Z",
      "dateUpdated": "2021-03-10T22:27:43.642Z",
      "amount": 0,
      "status": "string",
      "reason": "string",
      "userId": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
    }
  ],
  "balance": 0,
  "isAddFund": true
}
```

##### GET `/api/GetUserBalance/{id}`

##### PUT `/api/AddFund/{id}`
* For adding amount to User balance

##### PUT `/api/UpdateUserPayment/{userid}/{id}`

* Updating Payment Status such as "Active", "Pending", "Paid", "Closed"
* For active status for new payment
* For pending status for those can proces payment because of user account balance
* For Paid status this will update the User balance depending to amount of payment
* For Closed status this will result a successful payment





### How do I get set up? ###

* Try download or clone the project
* Open to Visual studio code
* Try to build and run 
* Browse the url https://localhost:5001/swagger/index.html to see available endpoint of the api




using System;
using System.Collections.Generic;

namespace BayarinApi
{

    public class User
    {
        public User()
        {
            Payments = new List<Payment>();
        }
        
        public Guid Id { get; set; }
 
        public string UserName { get; set; }
 
        public string Email { get; set; }
 

        public List<Payment> Payments  { get; set; }

        public double Balance { get; set; }

        public bool IsAddFund { get; set; }
    }
    public class Payment
    {
        public Guid Id { get; set; }        
 
        public string Name { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }

        public double Amount { get; set; }

        public string Status { get; set; }

        //public string PaymentMethod { get; set; }

        public string Reason { get; set; }

        public Guid UserId { get; set; } 
    }

    public enum PaymentStatus
    {
        //"Active", "Pending", "Paid", "Close"
        Active = 1,

        Pending = 2,

        Paid = 3,

        Closed = 4,

    }


}